package sn.kader.prestationapi.prestation.service;

import org.springframework.http.ResponseEntity;
import sn.kader.prestationapi.prestation.dto.PrestationResponse;

public interface PrestationService {
    ResponseEntity<PrestationResponse> getPrestations();
}
