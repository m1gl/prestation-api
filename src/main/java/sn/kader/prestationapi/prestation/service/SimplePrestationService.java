package sn.kader.prestationapi.prestation.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import sn.kader.prestationapi.prestation.dto.ContratRequest;
import sn.kader.prestationapi.prestation.dto.ContratResponse;
import sn.kader.prestationapi.prestation.dto.PrestationResponse;

@Slf4j
@Service
public class SimplePrestationService implements PrestationService {
    @Value("${server.port}")
    private String port;

    @Value("${contrat-api.host}")
    private String contractServerHost;

    @Override
    public ResponseEntity<PrestationResponse> getPrestations() {
        //recupere la liste des contrats (contrat-api)
        //contrats (post parametre userCode)
        RestTemplate restTemplate = new RestTemplate();

        ContratRequest contratRequest = new ContratRequest();
        HttpEntity<ContratRequest> contratRequestHttpEntity = new HttpEntity<>(contratRequest);
        contratRequest.setUserCode("test");
        String url = String.format("http://%s/contrats", contractServerHost);
        ResponseEntity<ContratResponse> responseEntity = restTemplate.exchange(url, HttpMethod.POST, contratRequestHttpEntity, ContratResponse.class);
        log.info(" reponse de contrat api {}",responseEntity);

        ContratResponse contratResponse = responseEntity.getBody();

        return ResponseEntity.ok(PrestationResponse.builder().nom("Cours").prix(500.).port(port).contrat(contratResponse).build());
    }
}
