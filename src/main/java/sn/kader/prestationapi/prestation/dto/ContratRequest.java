package sn.kader.prestationapi.prestation.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ContratRequest {
    private String userCode;
}
