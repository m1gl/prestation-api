package sn.kader.prestationapi.prestation.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class PrestationResponse {
    private String nom;
    private Double prix;
    private String port;
    private ContratResponse contrat;
}
