package sn.kader.prestationapi.prestation.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ContratResponse {
    private String title;
    private String content;
    private String date;
    private String port;
}