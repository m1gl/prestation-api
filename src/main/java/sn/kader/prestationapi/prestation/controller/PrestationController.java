package sn.kader.prestationapi.prestation.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import sn.kader.prestationapi.prestation.dto.PrestationResponse;
import sn.kader.prestationapi.prestation.service.PrestationService;

@RestController
public class PrestationController {
    private final PrestationService prestationService;

    public PrestationController(PrestationService prestationService) {
        this.prestationService = prestationService;
    }

    @GetMapping("/prestations")
    ResponseEntity<PrestationResponse> getPrestations(){

        return prestationService.getPrestations();
    }
}
