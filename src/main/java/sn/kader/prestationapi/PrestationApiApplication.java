package sn.kader.prestationapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrestationApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrestationApiApplication.class, args);
	}

}
